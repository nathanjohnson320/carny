defmodule CarnyStatementTest do
  use ExUnit.Case
  doctest Carny.Statement

  alias Carny.{Statement, Identifier}

  setup do
    {:ok,
     %{
       base_identifier: %Identifier{
         domain: "my-company",
         organization: "bulwark",
         resource: "deployments"
       }
     }}
  end

  test "passes?/2 is false for empty resources list", %{base_identifier: id} do
    refute Statement.passes?(id, %Statement{})
  end

  test "passes?/2 is true for a resource that passes a given statement", %{base_identifier: id} do
    assert Statement.passes?("start", id, %Statement{privileges: ["start"], resources: [id]})
  end

  test "passes?/2 is true for a resource that passes a given wildcard statement", %{
    base_identifier: id
  } do
    assert Statement.passes?("start", id, %Statement{
             privileges: ["start"],
             resources: [%{id | domain: "*"}]
           })
  end

  test "passes?/2 is false for a resource that fails any out of many statements", %{
    base_identifier: id
  } do
    refute Statement.passes?(id, %Statement{resources: [id, %{id | domain: "WRONG"}]})
  end

  test "passes?/2 is true for a resource that fails any out of many statements but is :deny", %{
    base_identifier: id
  } do
    refute Statement.passes?(id, %Statement{resources: [id, %{id | domain: "WRONG"}]})
  end

  test "passes?/2 is true for a resource that passes any out of many statements but is :deny", %{
    base_identifier: id
  } do
    assert Statement.passes?("start", id, %Statement{
             privileges: ["start"],
             resources: [id, %{id | domain: "*"}]
           })
  end
end
