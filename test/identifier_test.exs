defmodule CarnyIdentifierTest do
  alias Carny.Identifier

  use ExUnit.Case
  doctest Identifier

  setup do
    {:ok,
     %{
       base_identifier: %Identifier{
         domain: "my-company",
         organization: "bulwark",
         resource: "deployments"
       }
     }}
  end

  test "parse/1 parses an identifier string", %{base_identifier: id} do
    assert Identifier.parse("bulwark:my-company:deployments") == id
  end

  test "replace_wildcards/2 replaces organization wildcard", %{base_identifier: id} do
    assert %{id | organization: "*"} ==
             Identifier.replace_wildcards(id, %{id | organization: "*"})
  end

  test "replace_wildcards/2 replaces domain wildcard", %{base_identifier: id} do
    assert %{id | domain: "*"} == Identifier.replace_wildcards(id, %{id | domain: "*"})
  end

  test "replace_wildcards/2 replaces resource wildcard", %{base_identifier: id} do
    assert %{id | resource: "*"} == Identifier.replace_wildcards(id, %{id | resource: "*"})
  end

  test "replace_wildcards/2 replaces multiple wildcards", %{base_identifier: id} do
    assert Identifier.global() == Identifier.replace_wildcards(id, Identifier.global())
  end

  test "validate_against/2 returns true if the identifiers are compatible", %{
    base_identifier: id
  } do
    assert Identifier.validate_against(id, id)
  end

  test "validate_against/2 returns true if the identifiers are compatible via wildcard", %{
    base_identifier: id
  } do
    assert Identifier.validate_against(id, %{id | domain: "*"})
  end

  test "validate_against/2 returns false if the identifiers are incompatible via wildcard", %{
    base_identifier: id
  } do
    assert Identifier.validate_against(%{id | domain: "plaid"}, %{id | organization: "*"}) == false
  end

  test "validate_against/2 returns false if the identifiers are incompatible", %{
    base_identifier: id
  } do
    assert Identifier.validate_against(id, %{id | domain: "wrong"}) == false
  end
end
