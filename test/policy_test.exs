defmodule CarnyPolicyTest do
  use ExUnit.Case
  doctest Carny.Policy

  alias Carny.{Policy, Statement, Identifier}

  setup do
    {:ok,
     %{
       id: %Identifier{
         domain: "my-company",
         organization: "bulwark",
         resource: "deployments"
       },
       policy: %Policy{
         statements: [
           %Statement{
             privileges: ["start"],
             resources: [
               %Identifier{
                 domain: "my-company",
                 organization: "bulwark",
                 resource: "deployments"
               }
             ]
           }
         ]
       }
     }}
  end

  test "add_statement/2 adds a statement to a policy" do
    assert Carny.Policy.add_statement(%Policy{}, %Statement{}) == %Policy{
             statements: [%Statement{}]
           }
  end

  test "add_statement/2 adds a statement to a policy with other statements" do
    assert Carny.Policy.add_statement(
             %Policy{statements: [%Statement{effect: :deny}]},
             %Statement{}
           ) == %Policy{
             statements: [%Statement{}, %Statement{effect: :deny}]
           }
  end

  test "authorized?/2 should pass when given a valid id and a generic policy", %{
    id: id,
    policy: policy
  } do
    assert Policy.authorized?("start", id, policy)
  end

  test "authorized?/2 should pass when given a valid id and a wildcard policy", %{
    id: id
  } do
    assert Policy.authorized?("start", id, %Policy{
             statements: [%Statement{privileges: ["start"], resources: [Identifier.global()]}]
           })
  end

  test "authorized?/2 should fail when given a valid id and an invalid policy", %{
    id: id
  } do
    refute Policy.authorized?("start", %{id | domain: "anything"}, %Policy{
             statements: [
               %Statement{resources: [%Identifier{}]}
             ]
           })
  end
end
