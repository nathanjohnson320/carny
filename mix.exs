defmodule Carny.MixProject do
  use Mix.Project

  def project do
    [
      app: :carny,
      version: "2.0.0",
      description: "Policy based RBAC validation",
      package: package(),
      elixir: "~> 1.9",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:ex_doc, ">= 0.0.0", only: :dev, runtime: false}
    ]
  end

  defp package() do
    [
      links: %{"GitLab" => "https://gitlab.com/nathanjohnson320/carny"},
      licenses: ["MIT"]
    ]
  end
end
